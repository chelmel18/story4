from django.shortcuts import render, redirect
from . import forms
from .models import Jadwal


# Create your views here.
def home(request):
    return render(request, 'HomeStory2.html')

def about(request):
    return render(request, 'AboutMeStory2.html')

def experiences(request):
    return render(request, 'ExperiencesStory2.html')

def portfolio(request):
    return render(request, 'PortfolioStory2.html')

def contact(request):
    return render(request, 'ContactStory2.html')

def schedule(request):
    return render(request, 'ScheduleStory5.html')

def scheduleform(request):
    form = forms.FormJadwal()
    if request.method == "POST":
        form = forms.FormJadwal(request.POST)
        if form.is_valid():
            form.save()
            return redirect('story4:myschedule')
    return render(request, 'ScheduleFormStory5.html', {'form':form})

def schedule(request):
    daftar_item = Jadwal.objects.order_by('tanggal', 'waktu')
    response = {
        'daftar_item' :daftar_item,
    }
    return render(request, 'ScheduleStory5.html', response)

def delete_item(request, id):
    Jadwal.objects.get(id = id).delete()
    return redirect('story4:myschedule')


