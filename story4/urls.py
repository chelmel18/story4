from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.home, name='myhome'),
    path('home/', views.home, name='myhome'),
    path('about/', views.about, name='myabout'),
    path('experiences/', views.experiences, name='myexperiences'),
    path('portfolio/', views.portfolio, name='myportfolio'),
    path('contact/', views.contact, name='mycontact'),
    path('schedule/', views.schedule, name='myschedule'),
    path('addschedule/', views.scheduleform, name='myform'),
    path('delschedule/<id>/', views.delete_item, name='mydelschedule'),
]