from django.db import models
from django.utils import timezone

# Create your models here.
class Jadwal(models.Model):
    namakegiatan = models.TextField(max_length= 75)
    hari = models.CharField(max_length=15)
    tanggal = models.DateField(default = timezone.now)
    waktu = models.TimeField(default= timezone.now)
    tempat = models.CharField(max_length=80)
    kategori = models.CharField(null=True,max_length = 50)

    def __str__(self):
        return "{}".format(self.namakegiatan)
