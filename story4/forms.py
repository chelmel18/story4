from . import models
from django import forms


class FormJadwal(forms.ModelForm):
    namakegiatan = forms.CharField(
        widget= forms.TextInput(
            attrs= {
                "class" : "txt-form-large",
                "required"  : True,
                "placeholder"   : "Nama Kegiatan",
            }
        )
    )
    hari = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form",
                "required"  : True,
                "placeholder" : "Hari",
            }
        )
    )
    tanggal = forms.DateField(
        widget= forms.SelectDateWidget(
            attrs = {
                "class" : "pilih-tanggal",
                "required" : True,
            }
        )

    )
    waktu = forms.TimeField(
        widget = forms.TimeInput(
            attrs = {
                "class" : "txt-form",
                "placeholder"  : "00:00",
                "required"  : True,
            }
        )
    )
    tempat = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form",
                "placeholder" : "Tempat",
                "required"  : True,
            }
        )
    )
    kategori = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form",
                "placeholder" : "Kategori",
                "required"  : True,
            }
        )
    )
    class Meta:
        model = models.Jadwal
        fields = [
            'namakegiatan', 'hari', 'tanggal', 'waktu', 'tempat', 'kategori',
        ]
